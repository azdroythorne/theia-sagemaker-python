#!/bin/bash

###
#
# OVERVIEW
# 
# This script installs the Eclipse Theia IDE on a Jupyter notebook server.  Currently
# this script installs Theia IDE 1.1.0.  The Theia IDE supports many VS Code extensions
# and they can be installed from the web UI using the IDE's extension manager.
#
# To learn more about Theia IDE please visit https://theia-ide.org/
# 
# NOTES
#
# During execution this script will retrieve configuration files from the internet along
# with various NodeJS and Jupyter packages.  If deploying a notebook into a confined
# network environment you will need to alter this script to have access to the Theia IDE
# source and related packages.
#
# Please note that Theia is not a supported AWS product but is an open source software.
# This script is only a demonstration of how to install Theia IDE with Amazon SageMaker 
# notebooks.
#
# Also note that, once started, the Theia IDE will be built in the background, 
# independently of this script.  As such please allow up to 5 minutes after the 
# notebook has started for installation to complete.
#
###

set -e

sudo -u ec2-user -i <<'EOP'
#####################################
## INSTALL THEIA IDE FROM SOURCE
#####################################
# login to ECR
# pull the theia-python image
#####################################
### Configure Theia defaults
#####################################
mkdir -p ${HOME}/SageMaker/.theia

image_name="theia-python"
aws_account=$(aws sts get-caller-identity --query Account --output text)
aws_region=$(aws configure get region)
image="${aws_account}.dkr.ecr.${aws_region}.amazonaws.com/${image_name}:latest"
cross_account_role_arn="arn:aws:iam::853836848612:role/aibench-mlops-sagemaker-cross-account-role"

# Get the login command from ECR and execute it directly
$(aws ecr get-login --region ${aws_region} --no-include-email)

docker pull ${image}


cat >${HOME}/SageMaker/.theia/launch.json <<EOQ
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Current File",
            "type": "python",
            "request": "launch",
            "program": "${file}",
            "console": "internalConsole"
        }
    ]
}
EOQ

cat >${HOME}/SageMaker/.theia/settings.json <<EOS
{
    "workbench.iconTheme": "theia-file-icons",
    "terminal.integrated.inheritEnv": true,
    "python.linting.pylintEnabled": true,
    "python.linting.flake8Enabled": true,
    "python.linting.pycodestyleEnabled": true,
    "python.linting.enabled": true,
    "python.venvPath": "/var/opt/theia/anaconda3/envs"
}
EOS


## Setup .gitconfig
sed -i '/helper =/c\        helper = !aws codecommit credential-helper --profile AutomationCodeCommitProfile $@' ${HOME}/.gitconfig

## Setup AWS config
cat >>${HOME}/.aws/config << EOY
[profile AutomationCodeCommitProfile]
region = ${aws_region}
role_arn = ${cross_account_role_arn}
credential_source=Ec2InstanceMetadata
output = json
EOY

#####################################
### Integrate Theia IDE with Jupyter
#####################################
## CONFIGURE JUPYTER PROXY TO MAP TO THE THEIA IDE
JUPYTER_ENV=/home/ec2-user/anaconda3/envs/JupyterSystemEnv
source /home/ec2-user/anaconda3/bin/activate JupyterSystemEnv
cat >>${JUPYTER_ENV}/etc/jupyter/jupyter_notebook_config.py <<EOC
c.ServerProxy.servers = {
  'theia': {
    'command': ['docker', 'run', '-v', '/home/ec2-user:/var/opt/theia', '-p', '{port}:8080', '${image}'],
    'environment': {},
    'absolute_url': False,
    'timeout': 120
  }
}
EOC
pip install jupyter-server-proxy pylint autopep8 yapf pyflakes pycodestyle 'python-language-server[all]'
jupyter serverextension enable --py --sys-prefix jupyter_server_proxy
jupyter labextension install @jupyterlab/server-proxy
conda deactivate
EOP

## RESTART THE JUPYTER SERVER
initctl restart jupyter-server --no-wait
