FROM ubuntu:20.04

# Ensure apt is up to date.
RUN apt-get update --fix-missing \
    && apt-get install -y curl python3-dev python3-pip build-essential libssl-dev git vim unzip
RUN update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1

RUN pip3 install pylint autopep8 yapf pyflakes pycodestyle 'python-language-server[all]'
# Install aws CLI.
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && ./aws/install


ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 10.22.0

# Install nvm with node and npm.
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.30.1/install.sh | bash

RUN bash -c "source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default"

ENV NODE_PATH $NVM_DIR/versions/node/$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

RUN mkdir /opt/theia
WORKDIR /opt/theia
RUN npm install -g yarn
ENV NODE_OPTIONS --max_old_space_size=4096
COPY package.json package.json
RUN yarn
RUN yarn theia build
RUN mkdir -p /var/opt/theia
ENV THEIA_DEFAULT_PLUGINS=local-dir:/opt/theia/plugins
ENV THEIA_WEBVIEW_EXTERNAL_ENDPOINT={{hostname}}
CMD yarn --cwd /opt/theia start /var/opt/theia --hostname 0.0.0.0 --port 8080
